package com.example.cm_smartcity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.cm_smartcity.DB.DatabaseHelper;
import com.example.cm_smartcity.adapters.CustomArrayAdapter;
import com.example.cm_smartcity.entities.Notas;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper db;

    ArrayList<Notas> listaItems;
    CustomArrayAdapter adapter;

    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(this);

        registerForContextMenu((ListView) findViewById(R.id.lista));

        listaItems = new ArrayList<>();
        lista = findViewById(R.id.lista);

        fillList();

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor c = db.listar();
                c.moveToPosition(position);

                int idAtual = c.getInt(c.getColumnIndex(db.ID));
                String tituloAtual = c.getString(c.getColumnIndex(db.Titulo));
                String nomeAtual = c.getString(c.getColumnIndex(db.Nome));
                String localAtual = c.getString(c.getColumnIndex(db.Local));
                String dataAtual = c.getString(c.getColumnIndex(db.Data));
                String horaAtual = c.getString(c.getColumnIndex(db.Hora));
                String descAtual = c.getString(c.getColumnIndex(db.Descricao));

                Bundle extras = new Bundle();

                Intent i = new Intent(MainActivity.this, InfoCompleta.class);

                extras.putInt("ID_AT", idAtual);
                extras.putString("TITULO_AT", tituloAtual);
                extras.putString("NOME_AT", nomeAtual);
                extras.putString("LOCAL_AT", localAtual);
                extras.putString("DATA_AT", dataAtual);
                extras.putString("HORA_AT", horaAtual);
                extras.putString("DESC_AT", descAtual);

                i.putExtras(extras);

                startActivity(i);

            }
        });
    }

    private void fillList(){

        listaItems = db.listarNotas();
        adapter = new CustomArrayAdapter(this, listaItems);
        lista.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    //MENU PRINCIPAL
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.pesquisa:
                Toast.makeText(MainActivity.this, "PESQUISAR", Toast.LENGTH_SHORT).show();
                return true;*/
            case R.id.add:
                Intent i = new Intent(MainActivity.this, AdicionarNota.class);
                startActivity(i);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //MENU CONTEXTUAL
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item_list, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int index = info.position;
        Cursor c = db.listar();
        c.moveToPosition(index);

        int id_nota = c.getInt(c.getColumnIndex(db.ID));
        String titulo_nota = c.getString(c.getColumnIndex(db.Titulo));
        String nome_nota = c.getString(c.getColumnIndex(db.Nome));
        String local_nota = c.getString(c.getColumnIndex(db.Local));
        String data_nota = c.getString(c.getColumnIndex(db.Data));
        String hora_nota = c.getString(c.getColumnIndex(db.Hora));
        String desc_nota = c.getString(c.getColumnIndex(db.Descricao));

        Bundle extras = new Bundle();

        switch (item.getItemId()) {
            case R.id.edit:

                Intent i = new Intent(MainActivity.this, EditarNota.class);

                extras.putInt("PARAM_ID", id_nota);
                extras.putString("PARAM_NOME", nome_nota);
                extras.putString("PARAM_TITULO", titulo_nota);
                extras.putString("PARAM_LOCAL", local_nota);
                extras.putString("PARAM_DATA", data_nota);
                extras.putString("PARAM_HORA", hora_nota);
                extras.putString("PARAM_DESC", desc_nota);

                i.putExtras(extras);

                startActivity(i);

                finish();
                return true;

            case R.id.remove:
                deleteFromBD(id_nota);
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteFromBD(final int id){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setCancelable(true);
        builder.setTitle(getString(R.string.confirmacao));
        builder.setMessage(getString(R.string.apagar_nota_question));

        builder.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton(getString(R.string.eliminar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.apagarDaBD(id);
                Toast.makeText(MainActivity.this, R.string.nota_removida, Toast.LENGTH_SHORT).show();
                fillList();
            }
        });
        builder.show();

    }

}
