package com.example.cm_smartcity.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cm_smartcity.EditarItemRecycler;
import com.example.cm_smartcity.Encrypt.crypto;
import com.example.cm_smartcity.InfoCompleta;
import com.example.cm_smartcity.InfoItemRecycler;
import com.example.cm_smartcity.ListaProblemas;
import com.example.cm_smartcity.MainActivity;
import com.example.cm_smartcity.MySingleton;
import com.example.cm_smartcity.R;
import com.example.cm_smartcity.entities.Problema;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.O)
public class AdapterRecycler extends RecyclerView.Adapter<HolderRecycler> {

    Context c;
    ArrayList<Problema> problemas;

    public AdapterRecycler(Context c, ArrayList<Problema> problemas) {
        this.c = c;
        this.problemas = problemas;
    }

    @NonNull
    @Override
    public HolderRecycler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, null);
        c = view.getContext(); //SE OUVER ALGUM ERRO NA RECYCLER É DAQUI

        return new HolderRecycler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderRecycler holder, final int position) {
        holder.tx1.setText(problemas.get(position).getTituloP());
        holder.tx2.setText(problemas.get(position).getDescricaoP());
        holder.tx3.setText(problemas.get(position).getEstadoP());
        holder.imagem.setImageBitmap(problemas.get(position).getImagemP());
        final String perguntaDialog;

        if (problemas.get(position).getEstadoP().equals("Resolvido")){
            holder.tx3.setTextColor(c.getResources().getColor(R.color.colorGreen));
            perguntaDialog = "Pretende voltar a ativar este problema?";
        } else {
            holder.tx3.setTextColor(c.getResources().getColor(R.color.colorRed));
            perguntaDialog = "Pretende dar este problema como 'Resolvido'?";
        }

        //Vars usadas
        final int id_problema = problemas.get(position).getIdP();
        final String titulo_problema = problemas.get(position).getTituloP();
        final String descricao_problema = problemas.get(position).getDescricaoP();
        final String estado_problema = problemas.get(position).getEstadoP();

        LatLng latLng_problema = problemas.get(position).getLatlngP();
        final String latitude_problema = String.valueOf(latLng_problema.latitude);
        final String longitude_problema = String.valueOf(latLng_problema.longitude);

        final String data_problema = problemas.get(position).getDataP();

        final int id_utilizador_problema = problemas.get(position).getId_utilizador();

        Bitmap imagem_problema = problemas.get(position).getImagemP();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imagem_problema.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        final String encodedImage_prob = Base64.encodeToString(b , Base64.DEFAULT);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(c, problemas.get(position).getIdP()+" "+ problemas.get(position).getTituloP(), Toast.LENGTH_LONG).show();

                Bundle extras = new Bundle();

                Intent i = new Intent(c, InfoItemRecycler.class);

                extras.putInt("ID_PROB", id_problema);
                extras.putString("TITULO_PROB", titulo_problema);
                extras.putString("DESC_PROB", descricao_problema);
                extras.putString("ESTADO_PROB", estado_problema);
                extras.putString("LAT_PROB", latitude_problema);
                extras.putString("LNG_PROB", longitude_problema);
                extras.putString("DATA_PROB", data_problema);
                extras.putString("IMAGEM_PROB", encodedImage_prob);
                extras.putInt("USER_PROB", id_utilizador_problema);

                i.putExtras(extras);

                c.startActivity(i);
                //((ListaProblemas)c).finish();

            }
        });

        holder.tx3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setTitle("AVISO");
                builder.setMessage(perguntaDialog);

                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //Snackbar.make(view, "REMOVER",Snackbar.LENGTH_LONG).show();

                        String url = "https://pedroacm.000webhostapp.com/cm/cm/index.php/api/estado";

                        String novo_estado;
                        if (estado_problema.equals("Ativo")){
                            novo_estado = "Resolvido";
                        } else {
                            novo_estado = "Ativo";
                        }

                        Map<String, String> Jparams = new HashMap<>();
                        Jparams.put("id", crypto.encrypt(String.valueOf(id_problema)));
                        Jparams.put("estado", crypto.encrypt(novo_estado));

                        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(Jparams), new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (Boolean.parseBoolean(crypto.decrypt(response.getString("status")))) {
                                        //Snackbar.make(view, response.getString("msg"),Snackbar.LENGTH_SHORT).show();
                                        Toast.makeText(c, crypto.decrypt(response.getString("msg")), Toast.LENGTH_SHORT).show();
                                        ((ListaProblemas)c).finish();

                                        Bundle extras2 = new Bundle();
                                        Intent i = new Intent(c, ListaProblemas.class);
                                        extras2.putInt("ID_UTILIZADOR", id_utilizador_problema);
                                        i.putExtras(extras2);

                                        c.startActivity(i);
                                    } else{
                                        Snackbar.make(view, crypto.decrypt(response.getString("msg")),Snackbar.LENGTH_SHORT).show();
                                        //Toast.makeText(c, response.getString("msg"), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {}
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(c, "ERRO: " + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<String, String>();
                                headers.put("Content-Type", "application/json; charset=utf-8");
                                headers.put("User-agent", System.getProperty("http.agent"));
                                return headers;
                            }
                        };

                        MySingleton.getInstance(c).addToRequestQueue(jsonObjRequest);

                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(c, view);
                //inflating menu from xml resource
                popup.inflate(R.menu.menu_recycler);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.editRecycler:
                                //Snackbar.make(view, "EDITAR",Snackbar.LENGTH_LONG).show();

                                Bundle extrasEdit = new Bundle();

                                Intent i = new Intent(c, EditarItemRecycler.class);

                                extrasEdit.putInt("ID_PROB", id_problema);
                                extrasEdit.putString("TITULO_PROB", titulo_problema);
                                extrasEdit.putString("DESC_PROB", descricao_problema);
                                extrasEdit.putString("ESTADO_PROB", estado_problema);
                                //extrasEdit.putString("LAT_PROB", latitude_problema);
                                //extrasEdit.putString("LNG_PROB", longitude_problema);
                                extrasEdit.putString("DATA_PROB", data_problema);
                                extrasEdit.putString("IMAGEM_PROB", encodedImage_prob);
                                extrasEdit.putInt("USER_PROB", id_utilizador_problema);

                                i.putExtras(extrasEdit);

                                c.startActivity(i);
                                ((ListaProblemas)c).finish();

                                return true;
                            case R.id.removeRecycler:
                                //Snackbar.make(view, "REMOVER",Snackbar.LENGTH_LONG).show();

                                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                                builder.setTitle("AVISO");
                                builder.setMessage("De certeza que quer apagar este problema?");

                                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                        String url = "https://pedroacm.000webhostapp.com/cm/cm/index.php/api/delete";

                                        Map<String, String> Jparams = new HashMap<>();
                                        Jparams.put("id", crypto.encrypt(String.valueOf(id_problema)));

                                        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(Jparams), new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                try {
                                                    if (Boolean.parseBoolean(crypto.decrypt(response.getString("status")))) {
                                                        //Snackbar.make(view, response.getString("msg"),Snackbar.LENGTH_SHORT).show();
                                                        Toast.makeText(c, crypto.decrypt(response.getString("msg")), Toast.LENGTH_SHORT).show();
                                                        ((ListaProblemas)c).finish();

                                                        Bundle extras2 = new Bundle();
                                                        Intent i = new Intent(c, ListaProblemas.class);
                                                        extras2.putInt("ID_UTILIZADOR", id_utilizador_problema);
                                                        i.putExtras(extras2);

                                                        c.startActivity(i);
                                                    } else{
                                                        Snackbar.make(view, crypto.decrypt(response.getString("msg")),Snackbar.LENGTH_SHORT).show();
                                                        //Toast.makeText(c, response.getString("msg"), Toast.LENGTH_SHORT).show();
                                                    }

                                                } catch (JSONException e) {}
                                            }
                                        }, new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Toast.makeText(c, "ERRO: " + error.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                            @Override
                                            public Map<String, String> getHeaders() throws AuthFailureError {
                                                HashMap<String, String> headers = new HashMap<String, String>();
                                                headers.put("Content-Type", "application/json; charset=utf-8");
                                                headers.put("User-agent", System.getProperty("http.agent"));
                                                return headers;
                                            }
                                        };

                                        MySingleton.getInstance(c).addToRequestQueue(jsonObjRequest);


                                    }
                                });
                                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                                builder.show();

                                return true;
                            default:
                                return false;
                        }
                    }
                });
                //Popup aparece do lado esquerdo, pq?
                popup.show();

                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return problemas.size();
    }

}
