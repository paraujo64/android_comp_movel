package com.example.cm_smartcity.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cm_smartcity.R;

public class HolderRecycler extends RecyclerView.ViewHolder {

    ImageView imagem;
    TextView tx1, tx2, tx3;

    public HolderRecycler(@NonNull View itemView) {
        super(itemView);

        this.imagem = itemView.findViewById(R.id.ivRecycler);
        this.tx1 = itemView.findViewById(R.id.tituloRecycler);
        this.tx2 = itemView.findViewById(R.id.descRecycler);
        this.tx3 = itemView.findViewById(R.id.estadoRecycler);
    }
}
