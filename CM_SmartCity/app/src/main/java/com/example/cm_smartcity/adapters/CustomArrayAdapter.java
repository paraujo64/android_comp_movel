package com.example.cm_smartcity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.cm_smartcity.R;
import com.example.cm_smartcity.entities.Notas;

import java.util.ArrayList;

public class CustomArrayAdapter extends ArrayAdapter<Notas> {

    public CustomArrayAdapter(Context context, ArrayList<Notas> notas) {
        super(context, 0, notas);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Notas n = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_linha, parent, false);
        }

        ((TextView) convertView.findViewById(R.id.titulo)).setText(n.getTitulo());
        ((TextView) convertView.findViewById(R.id.local)).setText(n.getLocal());
        ((TextView) convertView.findViewById(R.id.data)).setText(n.getData());
        ((TextView) convertView.findViewById(R.id.hora)).setText(n.getHora());
        ((TextView) convertView.findViewById(R.id.nome)).setText(n.getNome());

        return convertView;
    }
}
