package com.example.cm_smartcity.Encrypt;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

@RequiresApi(api = Build.VERSION_CODES.O)
public class crypto {
    final static Base64.Encoder encorder = Base64.getEncoder();
    final static Base64.Decoder decorder = Base64.getDecoder();
    final static  String secretKey = "va65s4sd32avvas8d1va6s5d1a3sdfa5";

    static private Cipher cipher(int opmode) throws Exception{
        if(secretKey.length() != 32) throw new RuntimeException("SecretKey length is not 32 chars");
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec sk = new SecretKeySpec(secretKey.getBytes(), "AES");
        IvParameterSpec iv = new IvParameterSpec(secretKey.substring(0, 16).getBytes());
        c.init(opmode, sk, iv);
        return c;
    }
    static public String encrypt(String str){
        try{
            byte[] encrypted = cipher(Cipher.ENCRYPT_MODE).doFinal(str.getBytes("UTF-8"));
            return new String(encorder.encode(encrypted));
        }catch(Exception e){
            return null;
        }
    }
    static public String decrypt(String str){
        try{
            byte[] byteStr = decorder.decode(str.getBytes());
            return new String(cipher(Cipher.DECRYPT_MODE).doFinal(byteStr),"UTF-8");
        }catch(Exception e){
            return null;
        }
    }
}
