package com.example.cm_smartcity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cm_smartcity.Encrypt.crypto;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.O)
public class Registar extends AppCompatActivity {
    EditText newEmail, newNome, newPass, confirmPass;
    Button btnRegistar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registar);

        newEmail = findViewById(R.id.edtRegEmail);
        newNome = findViewById(R.id.edtRegNome);
        newPass = findViewById(R.id.edtRegPass);
        confirmPass = findViewById(R.id.edtRegConfPass);

        btnRegistar = findViewById(R.id.btnRegistar);

        btnRegistar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newEmail.getText().toString().matches("")||newNome.getText().toString().matches("")||newPass.getText().toString().matches("")||confirmPass.getText().toString().matches("")){
                    Toast.makeText(Registar.this, "Preencha todos os Campos!", Toast.LENGTH_SHORT).show();
                } else {
                    if(!newPass.getText().toString().equals(confirmPass.getText().toString())){
                        Toast.makeText(Registar.this, "A Password não corresponde!", Toast.LENGTH_SHORT).show();
                    } else {
                        registar();
                    }
                }
            }
        });
    }
    private void registar(){

        String url = "https://pedroacm.000webhostapp.com/cm/cm/index.php/api/registar";

        Map<String, String> paramsJ = new HashMap<>();
        paramsJ.put("nome", crypto.encrypt(newNome.getText().toString().trim()));
        paramsJ.put("email", crypto.encrypt(newEmail.getText().toString().trim()));
        paramsJ.put("password", crypto.encrypt(newPass.getText().toString().trim()));

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(paramsJ), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (Boolean.parseBoolean(crypto.decrypt(response.getString("status")))) {
                        Toast.makeText(Registar.this, "Utilizador Registado!", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(Registar.this, crypto.decrypt(response.getString("MSG")), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Registar.this, "ERRO: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);

    }
}
