package com.example.cm_smartcity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cm_smartcity.Encrypt.crypto;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.O)
public class EditarItemRecycler extends AppCompatActivity {
    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;

    ImageView imged;
    TextView tituloed, desced, estadoed, dataed;
    Button btneditar, btncancelar;

    int id_problema;
    int idLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_item_recycler);


        Intent intentLog = getIntent();
        Bundle extrasEdit = intentLog.getExtras();
        id_problema = extrasEdit.getInt("ID_PROB");
        String titulo_problema = extrasEdit.getString("TITULO_PROB");
        String descricao_problema = extrasEdit.getString("DESC_PROB");
        String estado_problema = extrasEdit.getString("ESTADO_PROB");
        String data_problema = extrasEdit.getString("DATA_PROB");
        String encodedImage_prob = extrasEdit.getString("IMAGEM_PROB");
        idLogin = extrasEdit.getInt("USER_PROB");

        byte[] decodedString = Base64.decode(encodedImage_prob, Base64.DEFAULT);
        Bitmap imagem_problema = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        imged = findViewById(R.id.imgEdit);
        tituloed = findViewById(R.id.tituloEdit);
        desced = findViewById(R.id.descEdit);
        //estadoed = findViewById(R.id.estadoEdit);
        dataed = findViewById(R.id.dataEdit);

        imged.setImageBitmap(imagem_problema);
        tituloed.setText(titulo_problema);
        desced.setText(descricao_problema);
        //estadoed.setText(estado_problema);
        dataed.setText(data_problema);

        /*if (estado_problema.equals("Resolvido")){
            estadoed.setTextColor(getResources().getColor(R.color.colorGreen));
        } else {
            estadoed.setTextColor(getResources().getColor(R.color.colorLighterRed));
        }*/

        /*
        imged.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camPermissions();
            }
        });*/

        btncancelar = findViewById(R.id.cancelarEdit);
        btneditar = findViewById(R.id.EditItem);

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Bundle extras2 = new Bundle();
                Intent i = new Intent(EditarItemRecycler.this, ListaProblemas.class);
                extras2.putInt("ID_UTILIZADOR", idLogin);
                i.putExtras(extras2);

                startActivity(i);
            }
        });

        btneditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //String imgSend = convertImage();

                if(tituloed.getText().toString().matches("")||desced.getText().toString().matches("")) {
                    Toast.makeText(EditarItemRecycler.this, "Preencha todos os Campos!", Toast.LENGTH_SHORT).show();
                } else{
                    editarProblema();

                }
            }
        });
    }

    private void camPermissions() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        } else {
            openCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CAMERA_PERM_CODE) {
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                openCamera();
            } else {
                Toast.makeText(this, "É necessária a permissão para a Camara!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void openCamera() {
        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(camera, CAMERA_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            Bitmap imagemm = (Bitmap) data.getExtras().get("data");
            imged.setImageBitmap(imagemm);
        }
    }

    private void editarProblema(){

        String url = "https://pedroacm.000webhostapp.com/cm/cm/index.php/api/editarP";

        Map<String, String> paramsJ = new HashMap<>();
        paramsJ.put("id", crypto.encrypt(String.valueOf(id_problema)));
        paramsJ.put("titulo", crypto.encrypt(tituloed.getText().toString()));
        paramsJ.put("descricao", crypto.encrypt(desced.getText().toString()));
        //paramsJ.put("imagem", img);

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(paramsJ), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (Boolean.parseBoolean(crypto.decrypt(response.getString("status")))) {
                        Toast.makeText(EditarItemRecycler.this, crypto.decrypt(response.getString("msg")), Toast.LENGTH_SHORT).show();
                        finish();

                        Bundle extras2 = new Bundle();
                        Intent i = new Intent(EditarItemRecycler.this, ListaProblemas.class);
                        extras2.putInt("ID_UTILIZADOR", idLogin);
                        i.putExtras(extras2);

                        startActivity(i);

                    } else {
                        Toast.makeText(EditarItemRecycler.this, crypto.decrypt(response.getString("msg")), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EditarItemRecycler.this, "ERRO: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);

    }

    public String convertImage(){
        imged.buildDrawingCache();
        Bitmap bm = imged.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b , Base64.DEFAULT);
        //base64.setText(encodedImage);
        //Toast.makeText(this, encodedImage,Toast.LENGTH_LONG).show();
        return encodedImage;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        Bundle extras2 = new Bundle();
        Intent i = new Intent(EditarItemRecycler.this, ListaProblemas.class);
        extras2.putInt("ID_UTILIZADOR", idLogin);
        i.putExtras(extras2);

        startActivity(i);

    }

}
