package com.example.cm_smartcity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cm_smartcity.Encrypt.crypto;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.O)
public class MapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {
    private GoogleMap mMap;

    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;

    public static final int REQUEST_CODE_LOCAL = 99;
    public static final int REQUEST_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;

    private AddressResultReceiver mResultReceiver;

    int idLogin;

    TextView tvTitulo;
    TextView tvDesc;
    EditText edtTit;
    EditText edtDesc;

    ImageView edtimg;

    ImageView imgImagem;

    //mapa para guardar informaçao extra no marker
    private Map<Marker, Bitmap> markersMap = new HashMap<Marker, Bitmap>();

    LatLng localAtual;

    //SENSOR - ACELAROMETRO
    private SensorManager sm;

    //vars para calculos de caelaração e gravidade
    private float acelVal;
    private float acelLast;
    private float shake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mResultReceiver = new AddressResultReceiver(new Handler());

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLastLocation();

        //OBTER O UTILIZADOR ATRAVES DO ID_UTILIZADOR
        Intent intentLog = getIntent();
        Bundle extraID_USER = intentLog.getExtras();
        idLogin= extraID_USER.getInt("ID_UTILIZADOR");

        FloatingActionButton fab = findViewById(R.id.fabMap);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras2 = new Bundle();
                Intent i = new Intent(MapActivity.this, ListaProblemas.class);
                extras2.putInt("ID_UTILIZADOR", idLogin);
                i.putExtras(extras2);

                startActivity(i);
                //finish();
            }
        });


        //SENSOR - ACELAROMETRO
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sm.registerListener(sensorListener, sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

        acelVal = SensorManager.GRAVITY_EARTH;
        acelLast = SensorManager.GRAVITY_EARTH;
        shake = 0.00f;
    }

    private void fetchLastLocation() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCAL);
            return;
        } else {
            Task<Location> task = fusedLocationProviderClient.getLastLocation();
            task.addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null){
                        currentLocation = location;

                        //PREENCHE A VARIAVEL GLOBAL COM A LOCALIZACAO ATUAL
                        localAtual = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

                        //mMap.moveCamera(CameraUpdateFactory.newLatLng(localAtual));
                        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(localAtual, 13),5000, null);

                        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.mapa);
                        mapFragment.getMapAsync(MapActivity.this);
                        getProblema();

                        startIntentService(location);

                    }
                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        //tvTitulo =  findViewById(R.id.tituloMarker);

        mMap = googleMap;
        googleMap.setOnMapClickListener(this);
        googleMap.setOnMapLongClickListener(this);

        //POE A CAMARA NA LOCALIZACAO ATUAL
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(localAtual, 17));

        if (mMap != null){
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = getLayoutInflater().inflate(R.layout.info_marker, null);


                        tvTitulo = v.findViewById(R.id.tituloMarker);
                        tvTitulo.setText(marker.getTitle());
                        tvDesc = v.findViewById(R.id.descMarker);
                        tvDesc.setText(marker.getSnippet());
                        imgImagem = (ImageView) v.findViewById(R.id.imgMarker);

                        Bitmap imagem = markersMap.get(marker);
                        imgImagem.setImageBitmap(imagem);

                    //LatLng ll = marker.getPosition();
                    return v;
                    //return null;
                }
            });
            //LatLng testeInicio = new LatLng(41.694570,-8.830160);
        }

    }

    @Override
    public void onMapClick(LatLng latLng) {
        //Toast.makeText(this, "Lat: " + String.valueOf(latLng.latitude)
        //                               + " Long: "+String.valueOf(latLng.longitude), Toast.LENGTH_LONG).show();

        Location l = new Location("");
        l.setLatitude(latLng.latitude);
        l.setLongitude(latLng.longitude);

        startIntentService(l);

    }

    @Override
    public void onMapLongClick(final LatLng latLng) {
        /*Toast.makeText(this, "Lat: " + String.valueOf(latLng.latitude)
                + " Long: "+String.valueOf(latLng.longitude), Toast.LENGTH_LONG).show();*/


        AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
        final View customLayout = getLayoutInflater().inflate(R.layout.layout_dialog_marker, null);
        builder.setView(customLayout);
        builder.setTitle("Descreva o problema");


        edtimg = customLayout.findViewById(R.id.imgAdd);

        edtimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camPermissions();
            }
        });


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                edtTit = customLayout.findViewById(R.id.edtTituloDlg);
                edtDesc = customLayout.findViewById(R.id.edtDescDlg);
                //edtLocal = customLayout.findViewById(R.id.edtLocalDlg);

                //String lat_point = String.valueOf(latLng.latitude);
                //String long_point = String.valueOf(latLng.longitude);

                LatLng localizacao = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

                String lat_point = String.valueOf(localizacao.latitude);
                String long_point = String.valueOf(localizacao.longitude);


                String imgEnviar = converImage();


                //Toast.makeText(MapActivity.this,latLng+" "+ edtTit.getText().toString() + " " + edtDesc.getText().toString()+" "+ edtLocal.getText().toString(), Toast.LENGTH_LONG).show();
                if(edtTit.getText().toString().matches("")||edtDesc.getText().toString().matches("")) {
                    Toast.makeText(MapActivity.this, "Preencha todos os Campos!", Toast.LENGTH_SHORT).show();
                } else{
                    //Pasasar para a base de dados!!!
                    addProblema(lat_point, long_point, imgEnviar);

                }
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MapActivity.this, "NAO", Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void camPermissions() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, REQUEST_CODE);
        } else {
            openCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE) {
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                openCamera();
            } else {
                Toast.makeText(this, "É necessária a permissão!", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_CODE_LOCAL){
            fetchLastLocation();
        }
    }

    public void openCamera(){
        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(camera, CAMERA_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            edtimg.setImageBitmap(image);
        }
    }

    private void addProblema(String lat_point, String long_point, String img_point){

        Calendar calendar = Calendar.getInstance();
        final String currentData = DateFormat.getDateInstance(DateFormat.DEFAULT).format(calendar.getTime());

        String url = "https://pedroacm.000webhostapp.com/cm/cm/index.php/api/problema";

        Map<String, String> paramsJ = new HashMap<>();
        paramsJ.put("titulo", crypto.encrypt(edtTit.getText().toString().trim()));
        paramsJ.put("descricao", crypto.encrypt(edtDesc.getText().toString().trim()));
        paramsJ.put("imagem", crypto.encrypt(img_point));
        paramsJ.put("latitude", crypto.encrypt(lat_point));
        paramsJ.put("longitude", crypto.encrypt(long_point));
        paramsJ.put("estado", crypto.encrypt("Ativo"));
        paramsJ.put("data", crypto.encrypt(currentData));
        paramsJ.put("id_utilizador", crypto.encrypt(String.valueOf(idLogin)));


        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(paramsJ), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (Boolean.parseBoolean(crypto.decrypt(response.getString("status")))) {
                        //tvTitulo.setText("v");
                        getProblema();
                        Toast.makeText(MapActivity.this, "Problema Adicionado!", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(MapActivity.this, crypto.decrypt(response.getString("MSG")), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MapActivity.this, "ERRO: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);

    }


    private void getProblema(){

        String url = "https://pedroacm.000webhostapp.com/cm/cm/index.php/api/problemas";

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arr = response.getJSONArray("DATA");
                    for(int i = 0; i<arr.length(); i++){
                        JSONObject obj = arr.getJSONObject(i);
                        //Toast.makeText(MapActivity.this, obj.getString("id")+" "+ obj.getString("titulo")+ " "+ obj.getString("estado")+ " "+ obj.getString("data"), Toast.LENGTH_LONG).show();
                        //System.out.println(obj.getString("titulo"));
                        //System.out.println("---------------QUERO LER ISTO---------\n\n"+obj.getString("id")+" "+ obj.getString("titulo")+ " "+ obj.getString("estado")+ " "+ obj.getString("data")+"; \n\n");

                        LatLng latLng = new LatLng(Double.parseDouble(crypto.decrypt(obj.getString("latitude"))),Double.parseDouble(crypto.decrypt(obj.getString("longitude"))));
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        markerOptions.title(crypto.decrypt(obj.getString("titulo")));
                        markerOptions.snippet(crypto.decrypt(obj.getString("descricao")));
                        String img = crypto.decrypt(obj.getString("imagem"));

                        byte[] decodedString = Base64.decode(img, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                        /*
                        int id =obj.getInt("id");
                        String titulo =obj.getString("titulo");
                        String descricao = obj.getString("descricao");
                        String estado = obj.getString("estado");
                        String data = obj.getString("data");
                        int id_utilizador = obj.getInt("id_utilizador");

                        Problema p = new Problema(id, titulo, descricao, estado, latLng, data, decodedByte, id_utilizador);
                        problemas.add(p);*/


                        if(crypto.decrypt(obj.getString("estado")).equals("Resolvido")){
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                            Marker m = mMap.addMarker(markerOptions);
                            markersMap.put(m, decodedByte);
                        } else {
                            Marker m = mMap.addMarker(markerOptions);
                            markersMap.put(m, decodedByte);
                        }

                    }
                } catch (JSONException e) {}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MapActivity.this, "ERRO: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(jsonObjRequest);

    }

    public String converImage(){
        edtimg.buildDrawingCache();
        Bitmap bm = edtimg.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b , Base64.DEFAULT);
        //base64.setText(encodedImage);
        //Toast.makeText(this, encodedImage,Toast.LENGTH_LONG).show();
        return encodedImage;
    }

    protected void startIntentService(Location location){
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
        startService(intent);
    }

    private class AddressResultReceiver extends ResultReceiver {

        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            String mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            Toast.makeText(MapActivity.this, mAddressOutput, Toast.LENGTH_LONG).show();
        }
    }


    //SENSOR - ACELAROMETRO
    private final SensorEventListener sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            acelLast = acelVal;
            acelVal = (float) Math.sqrt((double) (x*x + y*y + z*z));
            float delta = acelVal - acelLast;
            shake = shake * 0.9f + delta;

            if (shake > 12){
                if (mMap.getMapType() == GoogleMap.MAP_TYPE_NORMAL){
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                } else if (mMap.getMapType() == GoogleMap.MAP_TYPE_SATELLITE){
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
            //SE MUDAR A PRECISÃO DO SENSOR...

        }
    };

}
