package com.example.cm_smartcity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cm_smartcity.DB.DatabaseHelper;

public class InfoCompleta extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_completa);

        //PREENCHER OS TEXT_FIELDS
        Intent intentI = getIntent();
        Bundle extras = intentI.getExtras();

        int id = extras.getInt("ID_AT");
        String nome = extras.getString("NOME_AT");
        String titulo = extras.getString("TITULO_AT");
        String local = extras.getString("LOCAL_AT");
        String data = extras.getString("DATA_AT");
        String hora = extras.getString("HORA_AT");
        String descricao = extras.getString("DESC_AT");

        TextView mostraTitulo = (TextView)findViewById(R.id.mostraTitulo);
        TextView mostraNome = (TextView)findViewById(R.id.mostraNome);
        TextView mostraLocal = (TextView)findViewById(R.id.mostraLocal);
        TextView mostraData = (TextView)findViewById(R.id.dataAtual);
        TextView mostraHora = (TextView)findViewById(R.id.horaAtual);
        TextView mostraDesc = (TextView)findViewById(R.id.mostraDesc);

        mostraDesc.setMovementMethod(new ScrollingMovementMethod());

        //Toast.makeText(InfoCompleta.this, id + " " + titulo + " " + nome + " " + local  + " " + data, Toast.LENGTH_SHORT).show();

        mostraTitulo.setText(titulo);
        mostraNome.setText(nome);
        mostraLocal.setText(local);
        mostraData.setText(data);
        mostraHora.setText(getString(R.string.as) + " " + hora);
        mostraDesc.setText(descricao);

        //BOTAO PARA SAIR
        Button btnSair = (Button) findViewById(R.id.btnSair);

        btnSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
