package com.example.cm_smartcity.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.cm_smartcity.entities.Notas;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "Notas.db";
    public static final String DB_TABLE = "NotasTable";

    //COLUMNS
    public static final String ID = "ID";
    public static final String Titulo = "Titulo";
    public static final String Local = "Local";
    public static final String Data = "Data";
    public static final String Hora = "Hora";
    public static final String Nome = "Nome";
    public static final String Descricao = "Descricao";

    private static final String CREATE_TABLE = "CREATE TABLE "+DB_TABLE+" ("+
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            Titulo+ " TEXT, "+
            Local+ " TEXT, "+
            Data+ " TEXT, "+
            Hora+ " TEXT, "+
            Nome+ " TEXT, "+
            Descricao+ " TEXT "+ ")";

    public DatabaseHelper(Context context){
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ DB_TABLE);

        onCreate(db);
    }

    public boolean inserirData(String titulo, String local, String data, String hora, String nome, String descricao){
        SQLiteDatabase bd = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Titulo, titulo);
        contentValues.put(Local, local);
        contentValues.put(Data, data);
        contentValues.put(Hora, hora);
        contentValues.put(Nome, nome);
        contentValues.put(Descricao, descricao);

        long result = bd.insert(DB_TABLE, null, contentValues);

        return result !=-1; //se o result = -1 NAO INSERE
    }

    //LISTAR TUDO
    public Cursor listar(){
        SQLiteDatabase bd = this.getReadableDatabase();
        String query = "SELECT * FROM "+DB_TABLE;
        Cursor cursor = bd.rawQuery(query, null);

        return cursor;
    }

    //LISTAR COM O ARRAY
    public ArrayList<Notas> listarNotas() {
        ArrayList<Notas> arrayList = new ArrayList<>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String query = "SELECT * FROM "+DB_TABLE;
        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()){
            int id = cursor.getInt(0);
            String titulo = cursor.getString(1);
            String local = cursor.getString(2);
            String data = cursor.getString(3);
            String hora = cursor.getString(4);
            String nome = cursor.getString(5);
            String descricao = cursor.getString(6);

            Notas nota = new Notas(titulo, local, data, hora, nome, descricao);

            arrayList.add(nota);
        }

        return arrayList;
    }

    public void apagarDaBD(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + DB_TABLE + " WHERE "
                + ID + " = '" + id + "'";
        db.execSQL(query);
    }

    public void editar(int id, String novoTitulo, String novoLocal, String novoNome, String novaDescricao){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + DB_TABLE + " SET "
                + Titulo + " = '" + novoTitulo + "', "
                + Local + " = '" + novoLocal + "', "
                + Nome + " = '" + novoNome + "', "
                + Descricao + " = '" + novaDescricao + "' "
                + "WHERE " + ID + " = '" + id + "'" ;
        //System.out.println(query);
        db.execSQL(query);
    }

}
