package com.example.cm_smartcity.entities;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

public class Problema {
    public int idP;
    public String tituloP;
    public String descricaoP;
    public String estadoP;
    public LatLng latlngP;
    public String dataP;
    public Bitmap imagemP;
    public  int id_utilizador;

    public Problema(int idP, String tituloP, String descricaoP, String estadoP, LatLng latlngP, String dataP, Bitmap imagemP, int id_utilizador) {
        this.idP = idP;
        this.tituloP = tituloP;
        this.descricaoP = descricaoP;
        this.estadoP = estadoP;
        this.latlngP = latlngP;
        this.dataP = dataP;
        this.imagemP = imagemP;
        this.id_utilizador = id_utilizador;
    }

    public Problema() {
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getTituloP() {
        return tituloP;
    }

    public void setTituloP(String tituloP) {
        this.tituloP = tituloP;
    }

    public String getDescricaoP() {
        return descricaoP;
    }

    public void setDescricaoP(String descricaoP) {
        this.descricaoP = descricaoP;
    }

    public String getEstadoP() {
        return estadoP;
    }

    public void setEstadoP(String estadoP) {
        this.estadoP = estadoP;
    }

    public LatLng getLatlngP() {
        return latlngP;
    }

    public void setLatlngP(LatLng latlngP) {
        this.latlngP = latlngP;
    }

    public String getDataP() {
        return dataP;
    }

    public void setDataP(String dataP) {
        this.dataP = dataP;
    }

    public Bitmap getImagemP() {
        return imagemP;
    }

    public void setImagemP(Bitmap imagemP) {
        this.imagemP = imagemP;
    }

    public int getId_utilizador() {
        return id_utilizador;
    }

    public void setId_utilizador(int id_utilizador) {
        this.id_utilizador = id_utilizador;
    }
}
