package com.example.cm_smartcity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cm_smartcity.Encrypt.crypto;
import com.example.cm_smartcity.adapters.AdapterRecycler;
import com.example.cm_smartcity.entities.Problema;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.O)
public class ListaProblemas extends AppCompatActivity {

    RecyclerView myRecycler;
    AdapterRecycler myAdapter;

    ArrayList<Problema> listaProblemas;

    int IDLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_problemas);

        listaProblemas = new ArrayList<>();

        //OBTER O UTILIZADOR ATRAVES DO ID_UTILIZADOR
        Intent intentLog = getIntent();
        Bundle extraID = intentLog.getExtras();
        IDLogin = extraID.getInt("ID_UTILIZADOR");

        getProblema();

        myRecycler = findViewById(R.id.recyclerView);
        myRecycler.setLayoutManager(new LinearLayoutManager(this));

    }

    private void getProblema(){

        String url = "https://pedroacm.000webhostapp.com/cm/cm/index.php/api/problemasUser";

        Map<String, String> Jparams = new HashMap<>();
        Jparams.put("id_utilizador", crypto.encrypt(String.valueOf(IDLogin)));

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(Jparams), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (Boolean.parseBoolean(crypto.decrypt(response.getString("status")))) {
                        JSONArray arr = response.getJSONArray("DATA");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);

                            LatLng latLng = new LatLng(Double.parseDouble(crypto.decrypt(obj.getString("latitude"))), Double.parseDouble(crypto.decrypt(obj.getString("longitude"))));

                            int id = Integer.parseInt(crypto.decrypt(obj.getString("id")));
                            String titulo = crypto.decrypt(obj.getString("titulo"));
                            String descricao = crypto.decrypt(obj.getString("descricao"));
                            String estado = crypto.decrypt(obj.getString("estado"));
                            String data = crypto.decrypt(obj.getString("data"));
                            int id_utilizador = Integer.parseInt(crypto.decrypt(obj.getString("id_utilizador")));

                            String img = crypto.decrypt(obj.getString("imagem"));
                            byte[] decodedString = Base64.decode(img, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                            Problema problema = new Problema(id, titulo, descricao, estado, latLng, data, decodedByte, id_utilizador);

                            listaProblemas.add(problema);

                        }

                        myAdapter = new AdapterRecycler(getApplicationContext(), listaProblemas);
                        myRecycler.setAdapter(myAdapter);
                    } else{
                        Toast.makeText(ListaProblemas.this, "Sem Problemas Adicionados!", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ListaProblemas.this, "ERRO: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(jsonObjRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        Bundle extras2 = new Bundle();
        Intent i = new Intent(ListaProblemas.this, MapActivity.class);
        extras2.putInt("ID_UTILIZADOR", IDLogin);
        i.putExtras(extras2);

        startActivity(i);

    }
}
