package com.example.cm_smartcity.entities;

import java.util.Date;

public class Notas {
    public String titulo;
    public String local;
    public String data;
    public String hora;
    public String nome;
    public String descricao;

    public Notas(String titulo, String local, String data, String hora,String nome, String descricao) {
        this.titulo = titulo;
        this.local = local;
        this.data = data;
        this.hora = hora;
        this.nome = nome;
        this.descricao = descricao;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getLocal() {
        return local;
    }

    public String getData() {
        return data;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() { return descricao; }

    public String getHora() { return hora; }
}
