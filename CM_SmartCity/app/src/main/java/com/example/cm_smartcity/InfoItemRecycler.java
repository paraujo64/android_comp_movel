package com.example.cm_smartcity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

public class InfoItemRecycler extends AppCompatActivity {
    ImageView imgv;
    TextView idtv, titulotv, desctv, estadotv, coordtv, datatv, utilizadortv, moradatv;

    private AddressResultReceiver mResultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_item_recycler);

        Intent intentLog = getIntent();
        Bundle extra = intentLog.getExtras();
        int id_problema = extra.getInt("ID_PROB");
        String titulo_problema = extra.getString("TITULO_PROB");
        String descricao_problema = extra.getString("DESC_PROB");
        String estado_problema = extra.getString("ESTADO_PROB");
        String latitude_problema = extra.getString("LAT_PROB");
        String longitude_problema = extra.getString("LNG_PROB");
        String data_problema = extra.getString("DATA_PROB");
        String encodedImage_prob = extra.getString("IMAGEM_PROB");
        int id_utilizador_problema = extra.getInt("USER_PROB");

        LatLng latLng_problema = new LatLng(Double.parseDouble(latitude_problema),Double.parseDouble(longitude_problema));

        byte[] decodedString = Base64.decode(encodedImage_prob, Base64.DEFAULT);
        Bitmap imagem_problema = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        imgv = findViewById(R.id.imgInfo);
        //idtv = findViewById(R.id.idInfo);
        titulotv = findViewById(R.id.tituloInfo);
        desctv = findViewById(R.id.descInfo);
        estadotv = findViewById(R.id.estadoInfo);
        //coordtv = findViewById(R.id.coordInfo);
        datatv = findViewById(R.id.dataInfo);
        //utilizadortv = findViewById(R.id.utlizadorInfo);
        moradatv = findViewById(R.id.moradaInfo);

        imgv.setImageBitmap(imagem_problema);
        //idtv.setText(String.valueOf(id_problema));
        titulotv.setText(titulo_problema);
        desctv.setText(descricao_problema);
        estadotv.setText(estado_problema);
        //coordtv.setText(latLng_problema.toString());
        datatv.setText(data_problema);
        //utilizadortv.setText(String.valueOf(id_utilizador_problema));

        if (estado_problema.equals("Resolvido")){
            estadotv.setTextColor(getResources().getColor(R.color.colorGreen));
        } else {
            estadotv.setTextColor(getResources().getColor(R.color.colorLighterRed));
        }

        mResultReceiver = new AddressResultReceiver(new Handler());

        Location location = new Location("");
        location.setLatitude(latLng_problema.latitude);
        location.setLongitude(latLng_problema.longitude);

        startIntentService(location);

    }


    protected void startIntentService(Location location){
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
        startService(intent);
    }

    private class AddressResultReceiver extends ResultReceiver {

        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            String mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            if (mAddressOutput.matches("SERVICE NOT AVAILABLE")){
                moradatv.setText("");
            } else {
                moradatv.setText(mAddressOutput);
            }
        }
    }


}
