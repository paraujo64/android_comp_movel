package com.example.cm_smartcity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cm_smartcity.DB.DatabaseHelper;

public class EditarNota extends AppCompatActivity {

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_nota);

        db = new DatabaseHelper(this);

        //PREENCHER OS EDITTEXT COM OS ANTERIORES
        Intent intentC = getIntent();
        Bundle extras = intentC.getExtras();

        final int id_anterior = extras.getInt("PARAM_ID");
        String nome_anterior = extras.getString("PARAM_NOME");
        String titulo_anterior = extras.getString("PARAM_TITULO");
        String local_anterior = extras.getString("PARAM_LOCAL");
        String data_anterior = extras.getString("PARAM_DATA");
        String hora_anterior = extras.getString("PARAM_HORA");
        String desc_anterior = extras.getString("PARAM_DESC");

        final EditText editTitulo = (EditText)findViewById(R.id.editarTitulo);
        final EditText editNome = (EditText)findViewById(R.id.editarNome);
        final EditText editLocal = (EditText)findViewById(R.id.editarLocal);
        TextView DataLayout = (TextView)findViewById(R.id.mostraData);
        TextView HoraLayout = (TextView)findViewById(R.id.mostraHora);
        final EditText editDesc = (EditText)findViewById(R.id.editarDescricao);

        editDesc.setMovementMethod(new ScrollingMovementMethod());

        editTitulo.setText(titulo_anterior);
        editNome.setText(nome_anterior);
        editLocal.setText(local_anterior);
        DataLayout.setText(data_anterior);
        HoraLayout.setText(getString(R.string.as) + " " + hora_anterior);
        editDesc.setText(desc_anterior);

        //BOTAO PARA EDITAR
        Button btnEditar = (Button) findViewById(R.id.btnEdit);

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String titulo = editTitulo.getText().toString();
                String local = editLocal.getText().toString();
                String nome = editNome.getText().toString();
                String descricao = editDesc.getText().toString();

                //Toast.makeText(EditarNota.this, " " + titulo +" " + local +" " + nome +" " +id_anterior, Toast.LENGTH_SHORT).show();

                if (!titulo.equals("") && !local.equals("") && !nome.equals("")){
                    db.editar(id_anterior, titulo, local, nome, descricao);
                    Toast.makeText(EditarNota.this, R.string.nota_adicionada, Toast.LENGTH_SHORT).show();

                    //PRECISO DE OUTRA MANEIRA DE DAR RELOAD A LISTA
                    Intent i = new Intent(EditarNota.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditarNota.this);

                    builder.setCancelable(true);
                    builder.setTitle(R.string.alerta);
                    builder.setMessage(getString(R.string.nao_deixe_campos_vazios));

                    builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }

            }
        });

        Button btnCanc = (Button) findViewById(R.id.btnCanc);

        btnCanc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditarNota.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(EditarNota.this, MainActivity.class);
        startActivity(i);
    }
}
