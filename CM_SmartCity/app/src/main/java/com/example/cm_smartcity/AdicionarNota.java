package com.example.cm_smartcity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cm_smartcity.DB.DatabaseHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AdicionarNota extends AppCompatActivity {

    DatabaseHelper db;
    Button closeButton;
    Button btnCancelar;
    EditText add_titulo;
    EditText add_nome;
    EditText add_local;
    EditText add_data;
    EditText add_desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_nota);

        db = new DatabaseHelper(this);

        closeButton = findViewById(R.id.btnClose);
        btnCancelar = findViewById(R.id.btnCancel);

        add_titulo = findViewById(R.id.edt_titulo);
        add_nome = findViewById(R.id.edt_nome);
        add_local = findViewById(R.id.edt_local);
        //add_data = findViewById(R.id.edt_data);
        add_desc = findViewById(R.id.edt_desc);

        add_desc.setMovementMethod(new ScrollingMovementMethod());

        //VAIS BUSCAR A DATA ATUAL
        Calendar calendar = Calendar.getInstance();
        final String currentData = DateFormat.getDateInstance(DateFormat.DEFAULT).format(calendar.getTime()); //POSSO MUDAR DATEFORMAT

        //VAI BUSCAR A HORA ATUAL
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        final String time = format.format(calendar.getTime());

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo = add_titulo.getText().toString();
                String local = add_local.getText().toString();
                //String data = add_data.getText().toString();
                String data = currentData;
                String hora = time;
                String nome = add_nome.getText().toString();
                String descricao = add_desc.getText().toString();

                if (!titulo.equals("") && !local.equals("") && !nome.equals("") && !descricao.equals("") && db.inserirData(titulo, local, data, hora, nome, descricao)){
                    Toast.makeText(AdicionarNota.this, R.string.adicionado, Toast.LENGTH_SHORT).show();

                    add_titulo.setText("");
                    add_nome.setText("");
                    add_local.setText("");
                    //add_data.setText("");
                    add_desc.setText("");

                    //PRECISO DE OUTRA MANEIRA DE DAR RELOAD A LISTA
                    Intent i = new Intent(AdicionarNota.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(AdicionarNota.this);

                    builder.setCancelable(true);
                    builder.setTitle(getString(R.string.alerta));
                    builder.setMessage(getString(R.string.preencha_campos));

                    builder.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AdicionarNota.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    //BACK BUTTON
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(AdicionarNota.this, MainActivity.class);
        startActivity(i);
    }
}
