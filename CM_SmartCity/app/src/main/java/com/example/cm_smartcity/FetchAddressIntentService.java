package com.example.cm_smartcity;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.transform.Result;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class FetchAddressIntentService extends IntentService {
    protected ResultReceiver mReceiver;

    public FetchAddressIntentService() {
        super("FetchAddressIntentService");
    }

    private void deliverResultToReceiver(int resultCode, String message){
        Bundle bundle = new Bundle();
        bundle.putString(Constants.RESULT_DATA_KEY, message);
        mReceiver.send(resultCode, bundle);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String errorMessage="";

        Location location = intent.getParcelableExtra(Constants.LOCATION_DATA_EXTRA);

        mReceiver = intent.getParcelableExtra(Constants.RECEIVER);

        List<Address> adresses = null;

        try {
            adresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1);
        } catch (IOException ioExceprion) {
            errorMessage = "SERVICE NOT AVAILABLE";
            Log.e(TAG, errorMessage, ioExceprion);
        } catch (IllegalArgumentException illegalArgumentException) {
            errorMessage = "INVALID LAT_LONG USED";
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + location.getLatitude() +
                     ", Longitude = " +
                        location.getLongitude(),
                    illegalArgumentException);
        }

        if (adresses == null || adresses.size() ==0) {
            if (errorMessage.isEmpty()) {
                errorMessage = "NO ADDRESS FOUND";
                Log.e(TAG, errorMessage);
            }
            deliverResultToReceiver(Constants.FAILURE_RESULT, errorMessage);
        } else {
            Address address = adresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<>();

            for (int i =0; i <= address.getMaxAddressLineIndex(); i++) { //FALTAVA AQUI UM =
                addressFragments.add(address.getAddressLine(i));//EXISTEM OUTRAS COISA QUE SE PODE IR BUSCAR
            }
            Log.i(TAG, "ADRESS FOUND");
            deliverResultToReceiver(Constants.SUCCESS_RESULT,
                    TextUtils.join(System.getProperty("line.separator"), addressFragments));
        }

    }
}
