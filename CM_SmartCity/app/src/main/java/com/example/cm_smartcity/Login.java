package com.example.cm_smartcity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cm_smartcity.Encrypt.crypto;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.O)
public class Login extends AppCompatActivity {

    Button btnNotas, btnLogin;
    EditText editTextEmail, editTextPass;
    TextView tvRegistar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Log.d("TOKEN ", " "+ FirebaseInstanceId.getInstance().getToken());
        FirebaseMessaging.getInstance().subscribeToTopic("allDevices");

        btnNotas = findViewById(R.id.btnNotas);
        btnLogin = findViewById(R.id.btnLogin);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPass = findViewById(R.id.editTextPass);

        tvRegistar = findViewById(R.id.textviewRegistar);

        tvRegistar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Registar.class);
                startActivity(i);
            }
        });


        btnNotas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, MainActivity.class);
                startActivity(i);
                //finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextEmail.getText().toString().matches("")||editTextPass.getText().toString().matches("")){
                    Toast.makeText(Login.this, "Preencha todos os Campos!", Toast.LENGTH_SHORT).show();
                } else {
                    login();
                }
            }
        });
    }

    private void login(){
        String url = "https://pedroacm.000webhostapp.com/cm/cm/index.php/api/login";

        Map<String, String> Jparams = new HashMap<>();
        Jparams.put("email", crypto.encrypt(editTextEmail.getText().toString().trim()));
        Jparams.put("password", crypto.encrypt(editTextPass.getText().toString().trim()));

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(Jparams), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (Boolean.parseBoolean(crypto.decrypt(response.getString("status")))) {
                        Toast.makeText(Login.this, "Login com Sucesso!", Toast.LENGTH_SHORT).show();

                        int id_utilizador = Integer.parseInt(crypto.decrypt(response.getString("id")));;
                        Bundle extras = new Bundle();
                        Intent i = new Intent(Login.this, MapActivity.class);
                        extras.putInt("ID_UTILIZADOR", id_utilizador);
                        i.putExtras(extras);

                        startActivity(i);
                        finish();

                    } else {
                        Toast.makeText(Login.this, crypto.decrypt(response.getString("msg")), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Login.this, "ERRO: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);
    }
}
